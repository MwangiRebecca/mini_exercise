package com.example.mini_exercise;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.mini_exercise.pictureperfect.CapeOfGoodHope;
import com.example.mini_exercise.pictureperfect.Kingston;
import com.example.mini_exercise.pictureperfect.Oslo;
import com.example.mini_exercise.pictureperfect.Paris;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openKingston(View view) {
        startActivity(new Intent(MainActivity.this, Kingston.class));
    }

    public void openParis(View view) {
        startActivity(new Intent(MainActivity.this, Paris.class));

    }

    public void openOslo(View view) {
        startActivity(new Intent(MainActivity.this, Oslo.class));

    }

    public void openCGH(View view) {
        startActivity(new Intent(MainActivity.this, CapeOfGoodHope.class));

    }
}